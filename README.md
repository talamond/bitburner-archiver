# Bitburner Archiver

This project is intended to be a simple tool for transferring batches of files between Bitburner environtments
and the desktop [(by way of the python build)](https://gitlab.com/talamond/bitburner-archiver-desktop).

### Quickstart

A reminder of the available flags and options can be found by running `bar.js -h`.

By default bar uses the clipboard for storing the archive if no other option is given. This is to make it
easy to copy/paste whole folders between environments.

Simple usage:

`bar.js -c /bin/`
This will archive the /bin/ folder and all it's contents into the clipboard, after which you can run:
`bar.js -x`
Which will read the archive from the clipboard and export it's contents (recreating the /bin/ folder and it's
contents). *This does not automatically delete anything but will automatically overwrite files*.

If you want to add additional things to the archive after creating it, you can use:
`bar.js -a /lib/`
This will add /lib/ to the archive while leaving the already archived /bin/ untouched.

If you want to check the contents of the archive to make sure it grabbed everything, then list the contents with:
`bar.js -l`

### Archive Files

The archiver supports using files for storing archives in addition to the clipboard.
If you want to use a file in the Bitburner environment (perhaps you grabbed it with wget?), then just add
`-f file.bar.txt` to the existing commands, for example `bar.js -x -f file.bar.txt`.

If you want to create an archive file on your desktop for easy sharing directly, there's a filepicker option
available by just adding `-e`, for instance `bar.js -e -c /bin/` will launch a file picker and let you create
the file on your desktop.